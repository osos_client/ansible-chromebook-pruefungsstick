This ansible project modifies an ubuntu based flash drive, so that students can use their own device in exam situations.

The initial idea was to use chromebooks (Acer R752T), in exam-situations. Many chromebooks however cannot boot with a standard kernel. So using flash drives based on this project https://www.digitale-nachhaltigkeit.unibe.ch/services_and_support/lernstick/index_eng.html does not work. 

There is a different solution, though.

We can create a usb-flash drive based on this project, that will boot almost any Chromebook https://cb-linux.github.io/breath/docs.html#/

To be more concreate we use this command to create the usb-flash drive.

`bash setup.sh cli ubuntu focal-20.04`

Once the stick is done, boot from it. Open a terminal and execute these command

`apt update`

`apt install ansible -y`

`/usr/bin/ansible-pull -i "localhost" -e ansible_python_interpreter=/usr/bin/python3 -d /var/lib/ansible -U https://gitlab.com/osos_client/ansible-chromebook-pruefungsstick.git`

The ansible script mainly does this:

* Create a new user student.
* Auto login that user.
* Create a firewall 
* Disable mounting other usb-devices
* Add a printer, where students can print their final document. (Acutally a PDF-printer...)
